# -*- coding: utf-8 -*-

from resources.lib import kodilogging
from resources.lib import plugin

import logging
import xbmcaddon
import requests

# Keep this file to a minimum, as Kodi
# doesn't keep a compiled copy of this
ADDON = xbmcaddon.Addon()
kodilogging.config()


config = {
    'api': {
        'protocol': 'http',
        'host': 'localhost',
        'port': 8080
    }
}

url = '/api/users/'

endpoint = config['api']['protocol'] \
           + '://' \
           + config['api']['host'] \
           + ':' \
           + str(config['api']['port']) \
           + url

response = requests.get(endpoint)
logging.info("response_status is {}".format(response.status_code))

plugin.run()
