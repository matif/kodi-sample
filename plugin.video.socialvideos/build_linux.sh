rm -rf dist
mkdir dist
cd dist
mkdir plugin.video.socialvideos
cd ../
cd dist/plugin.video.socialvideos
mkdir resources
cd ../../
cp -r ./resources dist/plugin.video.socialvideos/resources
cp ./.travis.yml dist/plugin.video.socialvideos
cp ./addon.xml dist/plugin.video.socialvideos
cp ./changelog.txt dist/plugin.video.socialvideos
cp ./LICENSE dist/plugin.video.socialvideos
cp ./main.py dist/plugin.video.socialvideos
 p ./README.md dist/plugin.video.socialvideos
zip -r dist/plugin.video.socialvideos.zip dist/plugin.video.socialvideos/
rm -rf dist/plugin.video.socialvideos/
echo "DONE!"

