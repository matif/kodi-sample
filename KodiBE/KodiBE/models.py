"""Models file foe KodiBE."""

from django.db import models
from .choices import SOURCE_CHOINCES

class BaseManager(models.Manager):
    """Base Manager for extra functions."""


class BaseModelMixin(models.Model):
    """Abstract Base Model with common properties."""

    objects = BaseManager()
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        """Meta object for Base Model."""

        abstract = True


class PlaylistsModelMixin(models.Model):
    """Fields and methods necessary to support the Playlists model."""

    playlists = models.ManyToManyField(
        'PlaylistModel',
        blank=True,
        db_table='kodibe_playlist_playlist',
        related_name='kodibe_playlist_playlist_set'
    )

    videos = models.ManyToManyField(
        'VideoLinkModel',
        blank=True,
        db_table='kodibe_playlist_video_link',
        related_name='video_link_set'
    )


    class Meta:
        """Meta object for Permissions Mixin."""

        abstract = True


class PlaylistModel(BaseModelMixin, PlaylistsModelMixin):
    """Playlist Model."""

    name = models.CharField(max_length=45)
    is_root = models.BooleanField(default=False)
    something_else = models.BooleanField(default=False)

    def __str__(self):
        """Display name attribute of Playlist."""
        return self.name

    class Meta:
        """Meta object for Playlist Model."""

        db_table = 'kodibe_playlist'
        verbose_name = "Playlist"
        verbose_name_plural = "Playlists"


class VideoLinkModel(BaseModelMixin):
    """Social Site Video Link Model."""

    source = models.CharField(max_length=45, choices=SOURCE_CHOINCES)
    is_direct = models.BooleanField(default=False)
    title = models.CharField(max_length=45)
    link = models.URLField()
    playlists = models.ManyToManyField(
        PlaylistModel,
        db_table='kodibe_video_link_playlist',
        related_name='kodibe_video_link_playlist_set'
    )

    def __str__(self):
        """Display title attribute of VideoLink."""
        return self.title

    class Meta:
        """Meta object for Playlist Model."""

        db_table = 'kodibe_video_link'
        verbose_name = "Video Link"
        verbose_name_plural = "Video Links"
 
class AuthCodeModel(models.Model):
    
    code = models.CharField(max_length=50)
    authenticated = models.BooleanField(default=False)
    
    def __str__(self):
        """Display title attribute of VideoLink."""
        return self.code