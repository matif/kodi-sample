"""Admin Panel for KodiBE."""

from django.contrib import admin
from allauth.socialaccount.models import SocialAccount, SocialApp, SocialToken
from allauth.account.models import EmailAddress
from django.db.models import Q, Count

from .models import PlaylistModel, VideoLinkModel


class PlaylistModelAdmin(admin.ModelAdmin):
    """Admin Model for Playlist."""

    def get_queryset(self, request):
        """Qverride so only parent Playlists are shown in admin."""
        print('here')
        queryset = super(PlaylistModelAdmin, self).get_queryset(request)
        queryset = queryset.annotate(
            # num_playlists=Count('playlists'),
            num_parent_playlist=Count('kodibe_playlist_playlist_set'),
            # num_videos=Count('videos')
        ).filter(
            # Q(num_playlists__gt=0)|
            # Q(num_videos__gt=0),
            num_parent_playlist=0
        )
        return queryset


class VideoLinkModelAdmin(admin.ModelAdmin):
    """Admin Model for Video Link."""


admin.site.register(PlaylistModel, PlaylistModelAdmin)
admin.site.register(VideoLinkModel, VideoLinkModelAdmin)

# unregistered until Scope is Clear.
# admin.site.unregister(SocialAccount)
# admin.site.unregister(SocialApp)
# admin.site.unregister(SocialToken)
# admin.site.unregister(EmailAddress)
