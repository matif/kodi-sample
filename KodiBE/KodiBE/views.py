"""Views file for KodiBE."""

from django.shortcuts import render
from django.template import loader

from KodiBE.models import PlaylistModel, VideoLinkModel, AuthCodeModel
from api.serializers import (UserSerializer)

def index(request):
    """Index view for KodiBE."""
    return render(request, 'pages/kodibe-index.html', {})

def activate(request):
	return render(request, 'KodiBE/activate.html', {})

def youtube(request):
	return render(request, 'KodiBE/youtube.html', {})