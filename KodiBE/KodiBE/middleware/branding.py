"""Middleware for branding."""

import re

from django.conf import settings


class SwaggerBrandingMiddleware:
    """Middleware for branding swagger."""

    def __init__(self, get_response):
        """One-time configuration and initialization."""
        self.get_response = get_response
        self.title = settings.SWAGGER_SETTINGS.get('TITLE', 'Swagger UI')
        self.logo = settings.SWAGGER_SETTINGS.get('LOGO', '/static'
                                                  '/rest_framework_swagger'
                                                  '/logo_small.png')
        self.logo_text = settings.SWAGGER_SETTINGS.get('LOGO_TEXT', 'swagger')

    def __call__(self, request):
        """Get response from request."""
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        response = self.get_response(request)

        # Code to be executed for each request/response after
        # the view is called.

        response = self.modify_title(response)
        response = self.modify_logo(response)
        response = self.remove_swagger_text(response)

        return response

    def modify_title(self, response):
        """Modify the title."""
        response.content = re.compile(
            r'<title>Swagger UI</title>'
        ).sub(
            '<title>{}</title>'.format(self.title),
            response.content.decode("utf-8")
        ).encode()

        return response

    def modify_logo(self, response):
        """Modify logo."""
        response.content = re.compile(
            r'<img src="/static/rest_framework_swagger/logo_small.png".*?>'
        ).sub(
            '<img src="{0}" alt="{1}">'.format(
                self.logo,
                self.logo_text
            ),
            response.content.decode("utf-8")
        ).encode()

        return response

    def remove_swagger_text(self, response):
        """Remove swagger text."""
        response.content = re.compile(
            r'<span>swagger</span>'
        ).sub(
            '<span>{}</span>'.format(self.logo_text),
            response.content.decode("utf-8")
        ).encode()

        return response
