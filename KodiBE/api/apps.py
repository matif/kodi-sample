"""App Config File."""


from django.apps import AppConfig


class ApiConfig(AppConfig):
    """Configuration for app API."""

    name = 'api'
