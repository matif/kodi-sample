"""Serializers for the API."""


from rest_framework import serializers
from django.contrib.auth.models import User, Group, Permission, ContentType

from KodiBE.models import PlaylistModel, VideoLinkModel, AuthCodeModel


class UserSerializer(serializers.HyperlinkedModelSerializer):
    """Serializers define the API representation."""

    url = serializers.HyperlinkedIdentityField(
        view_name="api:user-detail",
    )

    groups = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name='api:group-detail',
    )

    user_permissions = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name='api:permission-detail',
    )

    class Meta:
        """Meta Object for Serializer."""
        model = User
        fields = ['url', 'email', 'is_superuser', 'is_active', 'is_staff',
                  'groups', 'user_permissions']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    """Serializers define the API representation."""

    url = serializers.HyperlinkedIdentityField(
        view_name="api:group-detail",
    )

    user_set = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name='api:user-detail',
    )

    permissions = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name='api:permission-detail',
    )

    class Meta:
        """Meta Object for Serializer."""

        model = Group
        fields = ['url', 'name', 'user_set', 'permissions']


class PermissionSerializer(serializers.ModelSerializer):
    """Serializers define the API representation."""

    url = serializers.HyperlinkedIdentityField(
        view_name="api:permission-detail",
    )

    user_set = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name='api:user-detail',
    )

    group_set = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name='api:group-detail',
    )

    class Meta:
        """Meta Object for Serializer."""

        model = Permission
        fields = ['url', 'name', 'codename', 'user_set', 'group_set']


class ContentTypeSerializer(serializers.ModelSerializer):
    """Serializers define the API representation."""

    class Meta:
        """Meta Object for Serializer."""

        model = ContentType
        fields = ['name', 'app_label', 'model']


class PlaylistModelSerializer(serializers.HyperlinkedModelSerializer):
    """Serializers define the API representation."""

    url = serializers.HyperlinkedIdentityField(
        view_name="api:playlistmodel-detail",
    )

    playlists = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name='api:playlistmodel-detail',
    )

    videos = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name='api:videolinkmodel-detail',
    )

    class Meta:
        """Meta Object for PlaylistModelSerializer."""

        model = PlaylistModel
        fields = ('url', 'name', 'is_root', 'playlists', 'videos',
                  'created', 'modified')


class VideoLinkModelSerializer(serializers.HyperlinkedModelSerializer):
    """Serializers define the API representation."""

    url = serializers.HyperlinkedIdentityField(
        view_name="api:videolinkmodel-detail",
    )

    playlists = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name='api:playlistmodel-detail',
    )

    class Meta:
        """Meta Object for VideoLinkModelSerializer."""

        model = VideoLinkModel
        fields = ('url', 'source', 'is_direct', 'title', 'link',
                  'playlists', 'created')

class AuthCodeModelSerializer(serializers.ModelSerializer):
    """Serializers define the API representation."""
    class Meta:
        """Meta Object for VideoLinkModelSerializer."""

        model = AuthCodeModel
        fields = ('id' , 'code', 'authenticated')
