"""Views for the API."""


from rest_framework import viewsets
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.schemas import SchemaGenerator
from rest_framework.views import APIView
from rest_framework_swagger import renderers
from rest_framework.decorators import action
from django.contrib.auth.models import User, Group, Permission
from django.db.models import Q, Count

from KodiBE.models import PlaylistModel, VideoLinkModel, AuthCodeModel
from .serializers import (UserSerializer, GroupSerializer, PermissionSerializer,
                          PlaylistModelSerializer, VideoLinkModelSerializer, AuthCodeModelSerializer)
from django.http import HttpResponseRedirect

# Create your views here.
class UserViewSet(viewsets.ModelViewSet):
    """ViewSets define the view behavior."""

    permission_classes = permissions.AllowAny,

    queryset = User.objects.all()
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """API endpoint that allows groups to be viewed or edited."""

    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class PermissionViewSet(viewsets.ModelViewSet):
    """API endpoint that allows permissions to be viewed or edited."""

    queryset = Permission.objects.all()
    serializer_class = PermissionSerializer


class PlaylistModelViewSet(viewsets.ModelViewSet):
    """ViewSets define the view behavior."""

    permission_classes = permissions.AllowAny,
    queryset = PlaylistModel.objects.all()
    serializer_class = PlaylistModelSerializer

    def list(self, request):
        """GET - Show Playlists."""
        # queryset = PlaylistModel.objects.all()
        queryset = PlaylistModel.objects.all().annotate(
            # num_playlists=Count('playlists'),
            num_parent_playlist=Count('kodibe_playlist_playlist_set'),
            # num_videos=Count('videos')
        ).filter(
            # Q(num_playlists__gt=0)|
            # Q(num_videos__gt=0),
            num_parent_playlist=0
        )
        serialized = PlaylistModelSerializer(
            queryset,
            context={'request': request},
            many=True
        )
        return Response(serialized.data)

    def retrieve(self, request, pk=None):
        """GET - Show Playlist Detail."""
        obj = PlaylistModel.objects.get(id=pk)
        serialized = PlaylistModelSerializer(
            obj,
            context={'request': request},
        )
        return Response(serialized.data)


class VideoLinkModelViewSet(viewsets.ModelViewSet):
    """ViewSets define the view behavior."""

    permission_classes = permissions.AllowAny,

    queryset = VideoLinkModel.objects.all()
    serializer_class = VideoLinkModelSerializer


class SwaggerSchemaView(APIView):
    """Schema view for swagger."""
    permission_classes = permissions.AllowAny,
    renderer_classes = [
        renderers.OpenAPIRenderer,
        renderers.SwaggerUIRenderer
    ]

    def get(self, request):
        """Handle get request."""
        generator = SchemaGenerator(title='drfwapi')
        schema = generator.get_schema(request=request)

        return Response(schema)

class AuthCodeModelViewSet(viewsets.ModelViewSet):
    """ViewSets define the view behavior."""
    permission_classes = permissions.AllowAny,

    queryset = AuthCodeModel.objects.all()
    serializer_class = AuthCodeModelSerializer
    @action(methods=['post'], detail=False, url_path='save-code', url_name='save_code')
    def save_code(self, request):
        # Save code into our system.
        data = request.data
        code = data['code']
        codeObj = AuthCodeModel.objects.filter(code=code)

        if len(codeObj) == 0:
            c = AuthCodeModel(code= code)
            c.save()
        else:
            c = codeObj[0]

        serialized = AuthCodeModelSerializer(
            c,
            context={'request': request},
        )
        response = serialized.data
        return Response(response)

    @action(methods=['post'], detail=False, url_path='verify', url_name='verify')
    def verify(self, request):
        data = request.data
        code = data['code']
        # Get the records from code
        codeObj = AuthCodeModel.objects.filter(code=code)
        if len(codeObj) == 0:
            response = "Invalid code entered."
            return Response(response)
        else:
            c = codeObj[0]
            c.authenticated = True
            c.save()
            response = "Code verified Successfully..."
            serialized = AuthCodeModelSerializer(
                c,
                context={'request': request},
            )
            url = "http://localhost:8000/accounts/google/login?code="+c.code
            return HttpResponseRedirect(url)

class LoginAPIView(APIView):
    """API endpoint that allows users to login."""

    permission_classes = permissions.AllowAny,

    def get(self, request):
        """Get user info."""

    def post(self, request):
        """Login user."""


def logout(request):
    """Logout user."""
