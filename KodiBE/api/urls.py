"""URLs for the API."""

from django.conf.urls import url, include
from rest_framework import routers

from .views import (UserViewSet, SwaggerSchemaView,
                    GroupViewSet, PermissionViewSet,
                    PlaylistModelViewSet, VideoLinkModelViewSet, AuthCodeModelViewSet)


# Routers provide an easy way of automatically determining the URL conf.
ROUTER = routers.DefaultRouter()
ROUTER.register(r'codes', AuthCodeModelViewSet)
ROUTER.register(r'users', UserViewSet)
ROUTER.register(r'groups', GroupViewSet)
ROUTER.register(r'permissions', PermissionViewSet)
ROUTER.register(r'playlists', PlaylistModelViewSet)
ROUTER.register(r'video-link', VideoLinkModelViewSet)

urlpatterns = [
    url(r'^', include(ROUTER.urls)),
    url(r'^specs/', SwaggerSchemaView.as_view()),
]
